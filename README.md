# Happymodel Mobula 7 1s

---

* ELRS 3.2.0;
* Betaflight 4.3.1;
* Default configuration;

---

## Binding 

* Open this [link](https://www.expresslrs.org/2.0/hardware/spi-receivers/) and go to UID byte generator:

![UID Generator](assets/uid.png)

* Type your phrase on [UID Generator](https://www.expresslrs.org/2.0/hardware/spi-receivers/);

* Copy the array;

* Go to betaflight cli and paste it like so, and hit enter;

```bash
cli: set express_lrs_uid = 65,245,33,230,58,226
cli: save
```

* Wait for the reboot to complete;

* Got to betaflight cli;

```bash
cli: bind_rx
```

* Hit enter;

* Go to your TX, radio configuration and select expresslrs lua script and select **bind**, on upper right side of the screen shoud be shown a C;


---

## USE AT YOUR OWN RISK

**This is an experimental feature...**